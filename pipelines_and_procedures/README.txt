This README.txt explains the different pipelines that must be used to generate the different raster files. In each pipeline the mentioned files must be executed in order to generate the final product.



------------------------------------------------------------------------
pipeline to create a point cloud *.las with ground points and not ground points 
------------------------------------------------------------------------
These are the steps to the process:
	1) 1_Classify_ground_and_hag.json
	2) 2_Filter_ground_by_outlier_Filter.json
	3) 3_a_Improve_ground_and_not_ground_by_normals.json
	4) 3_b_improve_ground_and_not_ground_by_kdistance.json
	5) 3_c_improve_ground_and_not_ground_by_pmf_filter.json
	6) 4_Merge_sensor_1_and_sensor_2_data.json



------------------------------------------------------------------------
pipeline to create a DTM raster file *.tif  
------------------------------------------------------------------------
These are the steps to the process:
	1) 1_Classify_ground_and_hag.json
	2) 2_Filter_ground_by_outlier_Filter.json
	3) 3_a_Improve_ground_and_not_ground_by_normals.json
	4) 3_b_improve_ground_and_not_ground_by_kdistance.json
	5) 3_c_improve_ground_and_not_ground_by_pmf_filter.json
	6) 4_Merge_sensor_1_and_sensor_2_data.json
	7) 5_create_Z_ground_raster.json
	8) 7_Fill_empty_ground_cells_by_interpolating_with_neighboring_values.txt(necessary only if you want to fill the shadow areas of points)



------------------------------------------------------------------------
pipeline to create a intensity ground raster file *.tif  
------------------------------------------------------------------------
These are the steps to the process:
	1) 1_Classify_ground_and_hag.json
	2) 2_Filter_ground_by_outlier_Filter.json
	3) 3_a_Improve_ground_and_not_ground_by_normals.json
	4) 3_b_improve_ground_and_not_ground_by_kdistance.json
	5) 3_c_improve_ground_and_not_ground_by_pmf_filter.json
	6) 4_Merge_sensor_1_and_sensor_2_data.json
	7) 6_create_intensity_ground_raster.json
	8) 7_Fill_empty_ground_cells_by_interpolating_with_neighboring_values.txt (necessary only if you want to fill the shadow areas of points)
	


------------------------------------------------------------------------
pipeline to create a wheelchair obstacles raster file *.tif  
------------------------------------------------------------------------
These are the steps to the process:
	1) 1_Classify_ground_and_hag.json
	2) 2_Filter_ground_by_outlier_Filter.json
	3) 3_a_Improve_ground_and_not_ground_by_normals.json
	4) 3_b_improve_ground_and_not_ground_by_kdistance.json
	5) 3_c_improve_ground_and_not_ground_by_pmf_filter.json
	6) 4_Merge_sensor_1_and_sensor_2_data.json
	7) 7_Fill_empty_ground_cells_by_interpolating_with_neighboring_values.txt
	8) 8_create_wheelchair_obstacles_raster.json
	9) 10_Clip_wheelchair_obstacles.txt



------------------------------------------------------------------------
pipeline to create a pedestrian obstacles raster file *.tif  
------------------------------------------------------------------------
These are the steps to the process:
	1) 1_Classify_ground_and_hag.json
	2) 2_Filter_ground_by_outlier_Filter.json
	3) 3_a_Improve_ground_and_not_ground_by_normals.json
	4) 3_b_improve_ground_and_not_ground_by_kdistance.json
	5) 3_c_improve_ground_and_not_ground_by_pmf_filter.json
	6) 4_Merge_sensor_1_and_sensor_2_data.json
	7) 7_Fill_empty_ground_cells_by_interpolating_with_neighboring_values.txt
	8) 8_create_pedestrian_obstacles_raster.json
	9) 10_Clip_pedestrian_obstacles.txt