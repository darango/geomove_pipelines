***************************************************************************
Clip_pedestrian_obstacles_raster.txt
--------------------------------------
  Date                 : jul 2018
  Copyright            : © 2018 by CartoLAB
  Email 1              : darango at emapic dot com
  Email 2			   : luipir at gmail dot com
***************************************************************************
*																		  *
*	This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published	  *
* 	by the Free Software Foundation; either version 2 of the License, 	  *
*	or (at your option) any later version.								  *
*																		  *
***************************************************************************

 // Estimated time to run this script: 
 // PC: 4Gb RAM - Intel® Core™2 Quad CPU Q8200 @ 2.33GHz × 4

*******************

These GDAL algorithms are used to clip a pedestrian obstacles raster with 
the area studio mask

To do this, you must first have a raster mask containing the buildings 
(pixel values = 0) and the road (pixel values = 1). The algorithms must 
be executed in a correlative way via command line.

********************
********************

10.1- ---(This stage is not necessary if you have already created the 
clipped_extent.tif)---
Create a mask adapted to the study area by gdal_translate. Input 
coordinates are the corners ulx uly lrx lry of the study area (out_9.tif).
These coordinates can be seen in the properties of the raster 
layer in QGIS software.(Output: clipped_extent.tif)
gdal_translate -projwin 547284.875 4801507.66 547504.835 4801352.5 -ot 
Float32 -of GTiff <building_mask.tif>  <clipped_extent.tif>

10.2- Multiply out_9.tif (pedestrian_obstacles_raster.tif) and 
clipped_extent.tif. (Output: mask.tif)
gdal_calc --calc "A*B" --format GTiff --type Float32 --outfile <out_11_pedestrian_obstacles_masked.tif> -A <out_9_pedestrian_obstacles_raster.tif> --A_band 1 
-B <clipped_extent.tif> --B_band 1